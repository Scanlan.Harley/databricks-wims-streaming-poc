# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC # Wims Pipeline
# MAGIC 
# MAGIC Create a pipe line to process raw pub messages and result with currernt state and history of POs. Spark steaming completes all of the the loading, transformations, and inserting

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Tables
# MAGIC 
# MAGIC | Table      | Pipeline Pos | Pipeline source | Test Text     | Merge |PK |
# MAGIC | :---        |    :----:   | |    :----:   |          ---: |
# MAGIC | structreRawPath      | 1|raw| Every raw message with sctructs added  |[ ] | None |
# MAGIC | structreUUID   | 2|structreRawPath| Merge table to keep status of each UUID| [x] | WIMS,UUID
# MAGIC | structreWIMS   | 3|structreUUID| Wims Fact table with history. Has currernt state of PO as well as history      | [x]  | wimsId for BI_ACT_SW = Y

# COMMAND ----------

# MAGIC %fs 
# MAGIC 
# MAGIC ls dbfs:/dbfs/

# COMMAND ----------

# MAGIC %sql
# MAGIC -- delete from delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreUUID/`;
# MAGIC -- delete from delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreRawPath/`;

# COMMAND ----------

# MAGIC %md
# MAGIC # Code

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Imports and Vars

# COMMAND ----------


import pyspark.sql.functions as F
from pyspark.sql.types import *
# from datadog import initialize, statsd
import json
sqlContext.setConf("spark.sql.shuffle.partitions", "200")
sqlContext.setConf("spark.databricks.optimizer.adaptive.enabled", "true")
spark.conf.set("spark.databricks.io.cache.enabled", "true")

rootPath = 's3a://crt-supply-chain-datalake/s595730/wims/'
structreRawPath = 'structreRawPath/'
structreUUID ='structreUUID/'
structreWIMS = 'structreWIMS/'
checkPointPath = '_checkpoint'

tableSchema = 's3a://crt-supply-chain-datalake/s595730/tableSchema'



# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Create Tables

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Defne raw schema
# MAGIC 
# MAGIC Schema is copied from create table statement from large raw insert. Prevents conflicts when dataset is too small to accurately infer schema
# MAGIC 
# MAGIC .items is still a array of structs and that is annoying. Handled in view but complacates things 

# COMMAND ----------

schema = """
   `details` STRUCT<`order`: STRUCT<`advisoryDoorRange`: STRING, `cancelOnDate`: STRING, `cancelledDateTime`: STRING, `closedDateTime`: STRING, `createdDateTime`: STRING, `deliveryAppointmentDateTime`: STRING, `demandId`: STRING, `destinationDockId`: BIGINT, `expectedArrivalDate`: STRING, `expectedDeparture`: STRUCT<`date`: STRING, `type`: STRING>, `firstReceivedDateTime`: STRING, `freight`: STRUCT<`freightType`: STRING, `managedBy`: STRING, `paymentType`: STRING>, `inventoryAnalystName`: STRING, `inventoryAnalystNumber`: STRING, `itemAggregates`: STRUCT<`forwardBuy`: STRUCT<`extendedListCost`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `palletCount`: BIGINT, `unitCount`: BIGINT, `weightInTenThousandthsLbs`: BIGINT>, `promo`: STRUCT<`extendedListCost`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `palletCount`: BIGINT, `unitCount`: BIGINT, `weightInTenThousandthsLbs`: BIGINT>, `turn`: STRUCT<`extendedListCost`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `palletCount`: BIGINT, `unitCount`: BIGINT, `weightInTenThousandthsLbs`: BIGINT>>, `items`: ARRAY<STRUCT<`backorderAllowed`: BOOLEAN, `billableQuantity`: BIGINT, `billableQuantityDelta`: BIGINT, `currentQuantityInMasterPacks`: BIGINT, `firstReceivedDateTime`: STRING, `itemClass`: STRING, `itemCode`: STRING, `itemCosts`: STRUCT<`backhaulAmount`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `backhaulPerCase`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `billbackAmount`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `caseLastCostAmount`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `freightAllowanceAmount`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `freightAllowancePerCase`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `freightBillAmount`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `freightBillPerCase`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `itemUpDownAmount`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `itemUpDownComment`: STRING, `lastCostAmount`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `listCostAmount`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `offInvoiceAmount`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `paymentTermDiscountAmount`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `prepayAndAddAmount`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `supplierUpDownAmount`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `totalExtendedListCostAmount`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `totalItemCostAmount`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `transferWarehouseAverageCost`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>>, `itemDescription`: STRING, `itemDimensions`: STRUCT<`breakDownPallets`: DOUBLE, `freightUnit`: STRING, `masterPack`: BIGINT, `orderFactorRate`: STRING, `orderedPalletQuantityInTenThousandths`: BIGINT, `orderedVolumeInTenThousandthsCube`: BIGINT, `orderedWeightInTenThousandthsLbs`: BIGINT, `packSize`: STRING, `palletQuantity`: BIGINT, `palletTie`: BIGINT, `palletTier`: BIGINT, `putAwayPallets`: DOUBLE, `receivedPalletQuantityInTenThousandths`: BIGINT, `receivedVolumeInTenThousandthsCube`: BIGINT, `receivedWeightInTenThousandthsLbs`: BIGINT, `shippingPack`: BIGINT, `shippingPackBreakdown`: BIGINT, `supplierOrderFactor`: DOUBLE, `supplierOrderFactorInTenThousandths`: STRING, `variableWeightIndicator`: STRING, `volumeInTenThousandthsCube`: BIGINT, `weightInTenThousandthsLbs`: BIGINT>, `itemState`: STRING, `lastReceivedDateTime`: STRING, `orderingUPC`: STRING, `originalForwardBuyQuantity`: BIGINT, `originalPromoQuantity`: BIGINT, `originalTotalOrderedQuantity`: BIGINT, `originalTurnQuantity`: BIGINT, `outstandingOrderedQuantity`: BIGINT, `productClassCode`: STRING, `promoInvestmentFlag`: STRING, `protectionLevelCode`: STRING, `quantityAdjustmentAudits`: ARRAY<STRING>, `receiptNumbers`: ARRAY<BIGINT>, `receipts`: ARRAY<STRUCT<`billableQuantity`: BIGINT, `catchWeightInTenThousandthsPounds`: BIGINT, `receiptNumber`: BIGINT, `receivedDamagedQuantity`: BIGINT, `receivedQuantity`: BIGINT>>, `receivedDamagedQuantity`: BIGINT, `receivedQuantity`: BIGINT, `revisedForwardBuyQuantity`: BIGINT, `revisedPromoQuantity`: BIGINT, `revisedTotalOrderedQuantity`: BIGINT, `revisedTurnQuantity`: BIGINT, `splitShipmentStatus`: STRING, `tmsItemSequence`: BIGINT, `trustedItemSwitch`: STRING, `warehouseShippedQuantity`: BIGINT>>, `lastReceivedDateTime`: STRING, `legacyData`: STRUCT<`applicationId`: STRING, `fpmConfirmCode`: STRING, `freightPaymentType`: STRING, `freightType`: STRING, `iTradeNetworkFacilityId`: STRING, `isITrade`: STRING, `omiCarrierRemark`: STRING, `omiRemark1`: STRING, `omiRemark2`: STRING, `omiRemark3`: STRING, `omiRemark4`: STRING, `omiTrafficRemark`: STRING, `powsRequestId`: STRING, `prepayToCollectFlag`: STRING, `supplierPickupReferenceNumber`: STRING>, `loadId`: STRING, `omiId`: BIGINT, `orderCosts`: STRUCT<`freightAllowance`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `freightAllowanceType`: STRING, `freightCharge`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `freightChargeRate`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `freightChargeSource`: STRING, `freightChargeType`: STRING, `prepayAndAdd`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `supplierUpDownAdjustment`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>, `supplierUpDownAdjustmentType`: STRING, `totalExtendedListCost`: STRUCT<`hCents`: BIGINT, `quotedDecimal`: STRING>>, `orderDimensions`: STRUCT<`totalBreakDownPallets`: BIGINT, `totalOrderedPalletQuantityInTenThousandths`: BIGINT, `totalOrderedVolumeInTenThousandthsCube`: BIGINT, `totalOrderedWeightInTenThousandthsLbs`: BIGINT, `totalPutAwayPallets`: DOUBLE, `totalReceivedPalletQuantityInTenThousandths`: BIGINT, `totalReceivedVolumeInTenThousandthsCube`: BIGINT, `totalReceivedWeightInTenThousandthsLbs`: BIGINT>, `orderSource`: STRING, `orderState`: STRING, `orderTags`: ARRAY<STRING>, `orderType`: STRING, `orderedDateTime`: STRING, `originFacilityAliasId`: STRING, `originalArrivalDate`: STRING, `productClassCode`: STRING, `protectionLevelCode`: STRING, `revisedArrivalDate`: STRING, `shipmentStatus`: STRING, `supplier`: STRUCT<`bicepNumber`: STRING, `contactName`: STRING, `contactPhone`: STRING, `faxContactName`: STRING, `faxPhone1`: STRING, `faxPhone2`: STRING, `laneFreightOnBoard`: STRING, `laneOriginCity`: STRING, `laneOriginState`: STRING, `laneOriginZipCode`: STRING, `locationNumber`: BIGINT, `maxOrderQuantity`: BIGINT, `maxOrderUnit`: STRING, `minOrderQuantity`: BIGINT, `minOrderUnit`: STRING, `supplierApNumber`: STRING, `supplierBicepNumber`: STRING, `supplierName`: STRING, `supplierNumber`: STRING>, `supplierPickupReferenceNumber`: STRING, `terminal`: STRING, `tmsOrderId`: STRING, `totalOrderQuantity`: BIGINT, `trailerNumber`: STRING, `warehouse`: STRUCT<`legacyWarehouseCode`: STRING, `offsiteWarehouseAddress`: STRING, `offsiteWarehouseCode`: STRING, `offsiteWarehouseName`: STRING, `warehouseNumber`: STRING>, `wimsId`: STRING>>,
  `metadata` STRUCT<`emittedAtDateTime`: STRING, `environment`: STRING, `messageType`: STRING, `messageTypeVersion`: STRING, `source`: STRING, `uuid`: STRING>,
  `outboundMessageVersion` STRING
"""

# COMMAND ----------

# MAGIC %sql 
# MAGIC CREATE TABLE IF NOT EXISTS s595730.structreRawPath USING DELTA LOCATION 's3a://crt-supply-chain-datalake/s595730/wims/structreRawPath';
# MAGIC CREATE TABLE IF NOT EXISTS s595730.structreWIMS USING DELTA LOCATION 's3a://crt-supply-chain-datalake/s595730/wims/structreWIMS';
# MAGIC CREATE TABLE IF NOT EXISTS s595730.structreUUID USING DELTA LOCATION 's3a://crt-supply-chain-datalake/s595730/wims/structreUUID';
# MAGIC CREATE TABLE IF NOT EXISTS s595730.raw_wims USING DELTA LOCATION 's3a://stg-enc-data/sc_raw_intake/raw_wims/';

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Pipeline Code

# COMMAND ----------

# MAGIC %md
# MAGIC ### Define ForeachBatch functions

# COMMAND ----------

def structreRaw(df, epoch_id):
  '''
  Takes datafrom from streaming read, appys schema scuture and appends it to table.

        Parameters:
                df2 (dataframe): streaming read dataframe
                epoch_id (int): int

        Returns:
               NA
  '''   
  df.createOrReplaceTempView('dft')
  
  
  df2 =  spark.read.schema(schema).json(df.rdd.map(lambda r: r.event))
  df2 = df2.filter(df2.metadata.messageTypeVersion > 2.2)
  df2.write \
  .format("delta") \
  .mode("append") \
  .save(rootPath + structreRawPath )  
  
  

def structreUUID(df2, epoch_id):
  '''
  Takes dataframe from streaming read and merges into table.

        Parameters:
                df2 (dataframe): streaming read dataframe
                epoch_id (int): int

        Returns:
               NA
  ''' 
  df2.createOrReplaceTempView('temp')
  
  df2._jdf.sparkSession().sql("""with src as (
SELECT
distinct
*
  
FROM
  (
    select
      details.order.wimsId,
      metadata.emittedAtDateTime,
      metadata.uuid,
      a.*,
      row_number() OVER (
        PARTITION BY details.order.wimsId,
        metadata.uuid
        ORDER BY
          metadata.emittedAtDateTime DESC
      ) rank
    from
      temp a
    where
      1 = 1
  ) a
where
  1 = 1
  --and wimsId = 'L2105-090-385792'
  --and rank = 1
order by
  metadata.emittedAtDateTime desc )
  
  merge into delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreUUID/` tgt
  USING src 
  on tgt.wimsId = src.wimsId and tgt.uuid = src.uuid
  WHEN NOT MATCHED
  THEN INSERT *
  """)
  
  
def structreWims2(df2, epoch_id):
  df2.createOrReplaceTempView('temp')
  
  df2._jdf.sparkSession().sql("""with src1 as (
  SELECT
    wimsId,
    emittedAtDateTime,
    struct(details.*) details,
    struct(metadata.*) metadata,
    PO_STAT,
    case
      when rank2 = 1 then true
      else false
    end as BI_ACT_SW
  FROM
    (
      select
        distinct wimsId,
        emittedAtDateTime,
        a.*,
        case
          when details.order.orderState not in ('cancelled', 'closed', 'canceled') then 'A'
          ELSE 'C'
        END PO_STAT,
        row_number() OVER (
          PARTITION BY details.order.wimsId
          ORDER BY
            metadata.emittedAtDateTime DESC
        ) rank2
      from
       temp a -- delta.    where
        --   1 = 1
    ) a
  where
    1 = 1
    and  rank2 = 1
),
src as (
  select
    *
  from
    src1 src
  where
    1 = 1 --  and BI_ACT_SW = true
    --     and src.wimsId = 'L2105-066-394557'
    and BI_ACT_SW = true
    minus
  select
    a.*
  from
    delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreWIMS/` a --   and src.BI_ACT_SW = true
  where
    1 = 1 --     a.wimsId = 'L2105-066-394557'
   and BI_ACT_SW = true
) Merge into delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreWIMS/` tgt
USING src on tgt.wimsId = src.wimsId
--and tgt.wimsId = 'L2105-051-391578'
--and tgt.BI_ACT_SW = true
--and tgt.metadata.uuid = src.metadata.uuid
WHEN MATCHED 
--AND  tgt.BI_ACT_SW = src.BI_ACT_SW
then
UPDATE
set
  *
  WHEN NOT MATCHED THEN
INSERT
  *
  """)
 
 

def structreWims(df2, epoch_id):
  df2.createOrReplaceTempView('temp')
  
  df2._jdf.sparkSession().sql("""
  with src1 as (
  SELECT
    wimsId,
    emittedAtDateTime,
    struct(details.*) details,
    struct(metadata.*) metadata,
    PO_STAT,
    case
      when rank2 = 1 then true
      else false
    end as BI_ACT_SW
  FROM
    (
      select
        distinct wimsId,
        emittedAtDateTime,
        a.*,
        case
          when details.order.orderState not in ('cancelled', 'closed', 'canceled') then 'A'
          ELSE 'C'
        END PO_STAT,
        row_number() OVER (
          PARTITION BY details.order.wimsId
          ORDER BY
            metadata.emittedAtDateTime DESC
        ) rank2
      from
       temp a
    ) a
  where
    1 = 1
    and rank2 = 1
),
src as (
  select
    z.wimsId as mk,
    z.*
  from
    src1 z
  where
    1 = 1 --  and BI_ACT_SW = true
    --     and src.wimsId = 'L2105-066-394557'
    -- and BI_ACT_SW = true
  union all
  SELECT
    NULL as mk,
    a.*
  FROM
    delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreWIMS/` a
    JOIN src1 ON src1.wimsId = a.wimsId
  WHERE
    a.BI_ACT_SW = true
    AND src1.metadata.uuid <> a.metadata.uuid
) -- select * from src


Merge into delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreWIMS/` tgt USING src
on tgt.wimsId = src.mk

--and tgt.wimsId = 'L2105-051-391578'
and tgt.BI_ACT_SW = true
-- and tgt.metadata.uuid = src.metadata.uuid

WHEN MATCHED
AND tgt.BI_ACT_SW = true
and tgt.metadata.uuid <> src.metadata.uuid then
UPDATE
set
  tgt.bi_act_sw = false
  WHEN NOT MATCHED THEN
INSERT
  *
  """)

# COMMAND ----------



# COMMAND ----------

# MAGIC %md
# MAGIC ### Create/Start Delta Streams

# COMMAND ----------

# MAGIC %md 
# MAGIC #### Raw to Struct

# COMMAND ----------


spark.readStream \
  .format("delta") \
  .load("s3a://stg-enc-data/sc_raw_intake/raw_wims/") \
  .writeStream \
  .queryName("structreRaw") \
  .foreachBatch(structreRaw) \
  .trigger(processingTime='30 seconds') \
  .option("checkpointLocation", rootPath + checkPointPath + structreRawPath ) \
  .start()

# RAW TO structreUUID

spark.readStream \
  .format("delta") \
  .load("s3a://crt-supply-chain-datalake/s595730/wims/structreRawPath/") \
  .writeStream \
  .queryName("structreUUID") \
  .foreachBatch(structreUUID) \
  .option("checkpointLocation", rootPath + checkPointPath + 'structreUUID' ) \
  .trigger(processingTime='30 seconds') \
  .start()


spark.readStream \
  .format("delta") \
  .load("s3a://crt-supply-chain-datalake/s595730/wims/structreRawPath/") \
  .writeStream \
  .queryName("structreUUID") \
  .foreachBatch(structreUUID) \
  .option("checkpointLocation", rootPath + checkPointPath + 'structreUUID' ) \
  .trigger(processingTime='30 seconds') \
  .start()
        
#   .option("maxFilesPerTrigger", 100) \
# .trigger(processingTime='60 seconds') \

# COMMAND ----------

# MAGIC %md 
# MAGIC #### Raw Struct to UUID Struct

# COMMAND ----------

# RAW TO structreUUID

spark.readStream \
  .format("delta") \
  .load("s3a://crt-supply-chain-datalake/s595730/wims/structreRawPath/") \
  .writeStream \
  .queryName("structreUUID") \
  .foreachBatch(structreUUID) \
  .option("checkpointLocation", rootPath + checkPointPath + 'structreUUID' ) \
  .trigger(processingTime='300 seconds') \
  .start()



# COMMAND ----------

# structreUUID TO structreWIMS
spark.readStream \
  .format("delta") \
  .load("s3a://crt-supply-chain-datalake/s595730/wims/structreUUID/") \
  .writeStream \
  .queryName("structreWims") \
  .foreachBatch(structreWims) \
  .option("checkpointLocation", rootPath + checkPointPath + 'structreWIMS' ) \
  .trigger(processingTime='300 seconds') \
  .start()

# COMMAND ----------

# MAGIC %sql
# MAGIC select
# MAGIC wimsId,
# MAGIC emittedAtDateTime,
# MAGIC PO_STAT,
# MAGIC BI_ACT_SW
# MAGIC 
# MAGIC from
# MAGIC   delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreWIMS/`
# MAGIC where
# MAGIC   1 = 1 -- 
# MAGIC --  and metadata.uuid='59a6a59c-56ba-469d-baa6-66d4258da141'
# MAGIC   --and details.order.items.receiptNumbers is not null
# MAGIC   --and wimsid = 'L2104-054-351457'
# MAGIC  and bi_act_sw = true -- 
# MAGIC   
# MAGIC   --details.order.items.receiptNumbers[-1]
# MAGIC   

# COMMAND ----------

# MAGIC %sql
# MAGIC select count(distinct(details.order.wimsid )) from s595730.structrerawpath

# COMMAND ----------

# MAGIC %sql
# MAGIC select
# MAGIC bi_act_sw,
# MAGIC count(*)
# MAGIC from
# MAGIC   delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreWIMS/` -- where wimsId= 'L2104-085-373801'
# MAGIC   -- order by emittedAtDateTime desc
# MAGIC   --2021-05-19T13:45:35.034654Z
# MAGIC   -- where metadata.emittedAtDateTime>  '2021-05-19T13:45:35.034654Z'
# MAGIC  where 
# MAGIC  1=1
# MAGIC  and po_stat = 'A'
# MAGIC  --and bi_act_sw = true
# MAGIC --
# MAGIC -- 2021-05-19T20:15:14.67209Z
# MAGIC group by bi_act_sw
# MAGIC order by
# MAGIC   2 desc
# MAGIC   
# MAGIC 
# MAGIC -- 82020
# MAGIC --82024
# MAGIC -- 82566
# MAGIC 
# MAGIC 
# MAGIC -- 578315
# MAGIC --578411
# MAGIC --582721

# COMMAND ----------

# MAGIC %sql -- create or replace table  delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreWIMS/`
# MAGIC -- using delta
# MAGIC -- PARTITIONED BY (BI_ACT_SW)
# MAGIC -- as
# MAGIC  
# MAGIC   
# MAGIC   
# MAGIC   
# MAGIC   
# MAGIC   with src1 as (
# MAGIC   SELECT
# MAGIC     wimsId,
# MAGIC     emittedAtDateTime,
# MAGIC     struct(details.*) details,
# MAGIC     struct(metadata.*) metadata,
# MAGIC     PO_STAT,
# MAGIC     case
# MAGIC       when rank2 = 1 then true
# MAGIC       else false
# MAGIC     end as BI_ACT_SW
# MAGIC   FROM
# MAGIC     (
# MAGIC       select
# MAGIC         distinct wimsId,
# MAGIC         emittedAtDateTime,
# MAGIC         a.*,
# MAGIC         case
# MAGIC           when details.order.orderState not in ('cancelled', 'closed', 'canceled') then 'A'
# MAGIC           ELSE 'C'
# MAGIC         END PO_STAT,
# MAGIC         row_number() OVER (
# MAGIC           PARTITION BY details.order.wimsId
# MAGIC           ORDER BY
# MAGIC             metadata.emittedAtDateTime DESC
# MAGIC         ) rank2
# MAGIC       from
# MAGIC         delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreUUID/` a -- delta.    where
# MAGIC         --   1 = 1
# MAGIC     ) a
# MAGIC   where
# MAGIC     1 = 1
# MAGIC     
# MAGIC ),
# MAGIC src as (
# MAGIC   select
# MAGIC     *
# MAGIC   from
# MAGIC     src1 src
# MAGIC   where
# MAGIC     1 = 1 --  and BI_ACT_SW = true
# MAGIC     --     and src.wimsId = 'L2105-066-394557'
# MAGIC    -- and BI_ACT_SW = true
# MAGIC     minus
# MAGIC   select
# MAGIC     a.*
# MAGIC   from
# MAGIC     delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreWIMS/` a --   and src.BI_ACT_SW = true
# MAGIC   where
# MAGIC     1 = 1 --     a.wimsId = 'L2105-066-394557'
# MAGIC     and BI_ACT_SW = true
# MAGIC ) Merge into delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreWIMS/` tgt
# MAGIC USING src on tgt.wimsId = src.wimsId 
# MAGIC --and tgt.wimsId = 'L2105-051-391578'
# MAGIC --and tgt.BI_ACT_SW = true
# MAGIC and tgt.metadata.uuid = src.metadata.uuid
# MAGIC WHEN MATCHED --AND  tgt.BI_ACT_SW = src.BI_ACT_SW
# MAGIC then
# MAGIC UPDATE
# MAGIC set
# MAGIC   *
# MAGIC   WHEN NOT MATCHED THEN
# MAGIC INSERT
# MAGIC   *

# COMMAND ----------

# MAGIC %sql with src1 as (
# MAGIC   SELECT
# MAGIC     wimsId,
# MAGIC     emittedAtDateTime,
# MAGIC     struct(details.*) details,
# MAGIC     struct(metadata.*) metadata,
# MAGIC     PO_STAT,
# MAGIC     case
# MAGIC       when rank2 = 1 then true
# MAGIC       else false
# MAGIC     end as BI_ACT_SW
# MAGIC   FROM
# MAGIC     (
# MAGIC       select
# MAGIC         distinct wimsId,
# MAGIC         emittedAtDateTime,
# MAGIC         a.*,
# MAGIC         case
# MAGIC           when details.order.orderState not in ('cancelled', 'closed', 'canceled') then 'A'
# MAGIC           ELSE 'C'
# MAGIC         END PO_STAT,
# MAGIC         row_number() OVER (
# MAGIC           PARTITION BY details.order.wimsId
# MAGIC           ORDER BY
# MAGIC             metadata.emittedAtDateTime DESC
# MAGIC         ) rank2
# MAGIC       from
# MAGIC         delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreUUID/` a -- delta.    where
# MAGIC         --   1 = 1
# MAGIC     ) a
# MAGIC   where
# MAGIC     1 = 1
# MAGIC     and rank2 = 1
# MAGIC ),
# MAGIC src as (
# MAGIC   select
# MAGIC     z.wimsId as mk,
# MAGIC     z.*
# MAGIC   from
# MAGIC     src1 z
# MAGIC   where
# MAGIC     1 = 1 --  and BI_ACT_SW = true
# MAGIC     --     and src.wimsId = 'L2105-066-394557'
# MAGIC     -- and BI_ACT_SW = true
# MAGIC   union all
# MAGIC   SELECT
# MAGIC     NULL as mk,
# MAGIC     a.*
# MAGIC   FROM
# MAGIC     delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreWIMS/` a
# MAGIC     JOIN src1 ON src1.wimsId = a.wimsId
# MAGIC   WHERE
# MAGIC     a.BI_ACT_SW = true
# MAGIC     AND src1.metadata.uuid <> a.metadata.uuid
# MAGIC ) -- select * from src
# MAGIC Merge into delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreWIMS/` tgt USING src on tgt.wimsId = src.mk --and tgt.wimsId = 'L2105-051-391578'
# MAGIC --and tgt.BI_ACT_SW = true
# MAGIC -- and tgt.metadata.uuid = src.metadata.uuid
# MAGIC WHEN MATCHED
# MAGIC AND tgt.BI_ACT_SW = true
# MAGIC and tgt.metadata.uuid <> src.metadata.uuid then
# MAGIC UPDATE
# MAGIC set
# MAGIC   tgt.bi_act_sw = false
# MAGIC   WHEN NOT MATCHED THEN
# MAGIC INSERT
# MAGIC   *

# COMMAND ----------

# MAGIC %sql truncate table delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreWIMS/`

# COMMAND ----------

# MAGIC %sql
# MAGIC select
# MAGIC   wimsId,
# MAGIC   count(*)
# MAGIC   
# MAGIC   
# MAGIC   -- wimsid,emittedAtDateTime,metadata.uuid,po_stat,bi_act_sw
# MAGIC from
# MAGIC   delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreWIMS/`
# MAGIC where
# MAGIC   1 = 1 -- and PO_STAT = 'C'
# MAGIC  and BI_ACT_SW = true
# MAGIC --   and wimsId = 'L2105-062-410550' --  group by PO_STAT,wimsId,BI_ACT_SW
# MAGIC   group by
# MAGIC   wimsId
# MAGIC having
# MAGIC   count(*) >  1 --group by details.order.warehouse.warehousenumber
# MAGIC 
# MAGIC order by
# MAGIC  2 desc
# MAGIC   
# MAGIC -- 79918
# MAGIC 
# MAGIC -- 562090
# MAGIC   
# MAGIC   
# MAGIC   

# COMMAND ----------

# MAGIC %sql
# MAGIC --1
# MAGIC select 'structreUUID' as  name, count(*) from delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreUUID/` where 1=1
# MAGIC union all
# MAGIC select 'structreRawPath',  count(*) from delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreRawPath/` where 1=1
# MAGIC union all
# MAGIC select 'structreWIMS', count(*) from delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreWIMS/`  where bi_act_sw = true and po_stat <> 'C'

# COMMAND ----------

# MAGIC %sql
# MAGIC select count(*) from delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreRawPath/` 

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ## Create Views
# MAGIC 
# MAGIC Since all data is intact, mapping can be done in a single view using sql alone. Single Struct for header and single struct for dtl.
# MAGIC 
# MAGIC _*mapping stolen from v1*_

# COMMAND ----------

# MAGIC %sql --df = spark.sql("""
# MAGIC --     select items.itemCode,items.itemDimensions.weightInTenThousandthsLbs
# MAGIC -- create database s595730;
# MAGIC -- create or replace view s595730.wimsPO as
# MAGIC select
# MAGIC   wimsId,
# MAGIC   hdr,struct(dtl) dtl
# MAGIC from
# MAGIC   (
# MAGIC     select wimsId,
# MAGIC       struct(
# MAGIC         case
# MAGIC           when details.order.omiid is not null then details.order.omiid
# MAGIC           else details.order.wimsId
# MAGIC         end AS PO_PURCHASE_ORDER_NUMBER,
# MAGIC         details.order.supplier.supplierNumber AS PO_VENDOR_NUMBER,
# MAGIC         details.order.warehouse.legacyWarehouseCode AS PO_VENDOR_FACILITY,
# MAGIC         NULL AS PO_VENDOR_FACILITY_NAME,
# MAGIC         details.order.supplier.supplierName AS PO_VENDOR_NAME,
# MAGIC         NULL AS PO_VENDOR_NOTE,
# MAGIC         details.order.legacyData.omiCarrierRemark AS PO_HEB_CARRIER_COMMENT,
# MAGIC         details.order.trailerNumber AS PO_HEB_TRAILER_NUMBER,
# MAGIC         details.order.trailerNumber AS PO_HEB_TRAILER_NUMBER_X,
# MAGIC         NULL AS PO_HEB_ALT_SHIP_TO_FLAG,
# MAGIC         NULL AS PO_PLPO_INDICATOR,
# MAGIC         NULL AS PO_FIRST_RECEIVE_SWITCH,
# MAGIC         NULL AS PO_EDI_STATUS,
# MAGIC         NULL AS PO_PHONED,
# MAGIC         NULL AS PO_MAILED,
# MAGIC         NULL AS PO_CONFIRMED,
# MAGIC         NULL AS PO_VENDOR_FLAT_AMOUNT,
# MAGIC         NULL AS PO_FACILITY_SHIP_TO,
# MAGIC         NULL AS PO_FACILITY_SHIP_TO_NAME,
# MAGIC         NULL AS PO_FLAT_FREIGHT_FLAG,
# MAGIC         NULL AS PO_FLAT_FREIGHT,
# MAGIC         NULL AS PO_FUTURE_USE,
# MAGIC         details.order.inventoryAnalystName AS PO_BUYER_NAME,
# MAGIC         details.order.supplier.contactPhone AS PO_PHONE,
# MAGIC         details.order.supplier.contactPhone AS PO_PHONE_AC,
# MAGIC         details.order.supplier.contactPhone AS PO_PHONE_1,
# MAGIC         details.order.supplier.contactPhone AS PO_PHONE_2,
# MAGIC         details.order.supplier.contactName AS PO_CONTACT,
# MAGIC         NULL AS PO_AUTO_MISMATCH,
# MAGIC         details.order.shipmentStatus AS PO_STATUS,
# MAGIC         NULL AS PO_ORIGIN,
# MAGIC         NULL AS PO_PICKUP_POINT_IND1,
# MAGIC         details.order.supplier.laneOriginCity AS PO_PICKUP_POINT_CITY1,
# MAGIC         details.order.supplier.laneOriginState AS PO_PICKUP_POINT_STATE1,
# MAGIC         NULL AS PO_PICKUP_POINT_FOB_CODE1,
# MAGIC         NULL AS PO_PICKUP_POINT_IND2,
# MAGIC         NULL AS PO_PICKUP_POINT_CITY2,
# MAGIC         NULL AS PO_PICKUP_POINT_STATE2,
# MAGIC         NULL AS PO_PICKUP_POINT_FOB_CODE2,
# MAGIC         NULL AS PO_PICKUP_POINT_IND3,
# MAGIC         NULL AS PO_PICKUP_POINT_CITY3,
# MAGIC         NULL AS PO_PICKUP_POINT_STATE3,
# MAGIC         NULL AS PO_PICKUP_POINT_FOB_CODE3,
# MAGIC         NULL AS PO_PICKUP_POINT_IND4,
# MAGIC         NULL AS PO_PICKUP_POINT_CITY4,
# MAGIC         NULL AS PO_PICKUP_POINT_STATE4,
# MAGIC         NULL AS PO_PICKUP_POINT_FOB_CODE4,
# MAGIC         details.order.supplier.minOrderQuantity AS PO_MINIMUM_QUANTITY,
# MAGIC         details.order.supplier.minOrderUnit AS PO_MINIMUM_TYPE,
# MAGIC         NULL AS PO_CURRENT_BKT_QUANTITY,
# MAGIC         NULL AS PO_CURRENT_BKT_TYPE,
# MAGIC         details.order.supplier.maxOrderQuantity AS PO_MAXIMUM_QUANTITY,
# MAGIC         details.order.supplier.maxOrderUnit AS PO_MAXIMUM_TYPE,
# MAGIC         --   (CAST(details.order.orderCosts.totalExtendedListCost.quotedDecimal AS DOUBLE)/CAST(details.order.itemAggregates.turn.extendedListCost.quotedDecimal AS DOUBLE) AS PO_TURN_COST),
# MAGIC         details.order.itemAggregates.turn.extendedListCost.quotedDecimal AS PO_TURN_COST,
# MAGIC         --   CAST(new.ORDEREDWEIGHTINTENTHOUSANDTHSLBS AS DOUBLE)/CAST(details.order.itemAggregates.turn.weightInTenThousandthsLbs AS DOUBLE) AS PO_TURN_WEIGHT,
# MAGIC         details.order.itemAggregates.turn.weightInTenThousandthsLbs / 10000 AS PO_TURN_WEIGHT,
# MAGIC         --   new.ORDEREDVOLUMEINTENTHOUSANDTHSCUBE AS PO_TURN_CUBE,
# MAGIC         --ITEM_CUBE_ARRAY AS PO_TURN_CUBE,
# MAGIC         --   CAST(new.ORDEREDPALLETQUANTITYINTENTHOUSANDTHS AS DOUBLE) / CAST(details.order.itemAggregates.turn.palletCount AS DOUBLE)  AS PO_TURN_PALLETS,
# MAGIC         details.order.itemAggregates.turn.palletCount / 10000 AS PO_TURN_PALLETS,
# MAGIC         --   details.order.itemAggregates.turn.unitCount AS PO_TURN_UNITS,
# MAGIC         details.order.itemAggregates.turn.unitCount AS PO_TURN_UNITS,
# MAGIC         NULL AS PO_TURN_FACTOR_1,
# MAGIC         NULL AS PO_TURN_FACTOR_2,
# MAGIC         details.order.itemAggregates.turn.extendedListCost.quotedDecimal AS PO_TURN_COST_ALT,
# MAGIC         details.order.itemAggregates.turn.weightInTenThousandthsLbs / 10000 AS PO_TURN_WEIGHT_ALT,
# MAGIC         --ITEM_CUBE_ARRAY AS PO_TURN_CUBE_ALT,
# MAGIC         details.order.itemAggregates.turn.palletCount AS PO_TURN_PALLETS_ALT,
# MAGIC         details.order.itemAggregates.turn.unitCount AS PO_TURN_UNITS_ALT,
# MAGIC         NULL AS PO_TURN_FACTOR_1_ALT,
# MAGIC         NULL AS PO_TURN_FACTOR_2_ALT,
# MAGIC         details.order.itemAggregates.turn.extendedListCost.quotedDecimal AS PO_ACTUAL_TURN_COST,
# MAGIC         --   new.ORDEREDWEIGHTINTENTHOUSANDTHSLBS AS PO_ACTUAL_TURN_WEIGHT,
# MAGIC         details.order.itemAggregates.turn.weightInTenThousandthsLbs / 10000 AS PO_ACTUAL_TURN_WEIGHT,
# MAGIC         --   new.ORDEREDVOLUMEINTENTHOUSANDTHSCUBE AS PO_ACTUAL_TURN_CUBE,
# MAGIC         --ITEM_CUBE_ARRAY AS PO_ACTUAL_TURN_CUBE,
# MAGIC         --   new.ORDEREDPALLETQUANTITYINTENTHOUSANDTHS  AS PO_ACTUAL_TURN_PALLETS,
# MAGIC         details.order.itemAggregates.turn.palletCount / 10000 AS PO_ACTUAL_TURN_PALLETS,
# MAGIC         details.order.itemAggregates.turn.unitCount AS PO_ACTUAL_TURN_UNITS,
# MAGIC         NULL AS PO_ACTUAL_TURN_FACTOR_1,
# MAGIC         NULL AS PO_ACTUAL_TURN_FACTOR_2,
# MAGIC         details.order.itemAggregates.promo.extendedListCost.quotedDecimal AS PO_PROMO_COST,
# MAGIC         details.order.itemAggregates.promo.weightInTenThousandthsLbs / 10000 AS PO_PROMO_WEIGHT,
# MAGIC         NULL AS PO_PROMO_CUBE,
# MAGIC         details.order.itemAggregates.promo.palletCount AS PO_PROMO_PALLETS,
# MAGIC         --   CAST(new.ORIGINALPROMOQUANTITY AS DOUBLE) /CAST(details.order.itemAggregates.promo.unitCount AS DOUBLE) AS PO_PROMO_UNITS,
# MAGIC         details.order.itemAggregates.turn.unitCount AS PO_PROMO_UNITS,
# MAGIC         NULL AS PO_PROMO_FACTOR_1,
# MAGIC         NULL AS PO_PROMO_FACTOR_2,
# MAGIC         details.order.itemAggregates.forwardBuy.extendedListCost.quotedDecimal AS PO_FWD_BUY_COST,
# MAGIC         --   details.order.itemAggregates.forwardBuy.weightInTenThousandthsLbs AS PO_FWD_BUY_WEIGHT,
# MAGIC         details.order.itemAggregates.forwardBuy.weightInTenThousandthsLbs / 10000 AS PO_FWD_BUY_WEIGHT,
# MAGIC         NULL AS PO_FWD_BUY_CUBE,
# MAGIC         details.order.itemAggregates.forwardBuy.palletCount AS PO_FWD_BUY_PALLETS,
# MAGIC         --   CAST(new.ORIGINALFORWARDBUYQUANTITY AS DOUBLE)/CAST(details.order.itemAggregates.forwardBuy.unitCount AS DOUBLE) AS PO_FWD_BUY_UNITS,
# MAGIC         details.order.itemAggregates.forwardBuy.unitCount AS PO_FWD_BUY_UNITS,
# MAGIC         NULL AS PO_FWD_BUY_FACTOR_1,
# MAGIC         NULL AS PO_FWD_BUY_FACTOR_2,
# MAGIC         NULL AS PO_ORDER_INTERVAL_WEEKS,
# MAGIC         NULL AS PO_LEAD_TIME_STATED_WEEKS,
# MAGIC         NULL AS PO_LEAD_TIME_AVERAGE_WEEKS,
# MAGIC         NULL AS PO_ACTIVE_CONTRACT,
# MAGIC         details.order.orderCosts.freightAllowance.quotedDecimal AS PO_FREIGHT_ALLOW,
# MAGIC         details.order.orderCosts.freightAllowanceType AS PO_FREIGHT_ALLOW_TYPE,
# MAGIC         NULL AS PO_FREIGHT_ALLOW_EX,
# MAGIC         NULL AS PO_FREIGHT_ALLOW_EX_TYPE,
# MAGIC         details.order.revisedArrivalDate AS PO_REVISED_ARRIVAL_MM,
# MAGIC         details.order.revisedArrivalDate AS PO_REVISED_ARRIVAL_DD,
# MAGIC         details.order.revisedArrivalDate AS PO_REVISED_ARRIVAL_YY,
# MAGIC         --   NULL AS PO_REVISED_ARRIVAL_CONCAT,
# MAGIC         --   NULL AS v_PO_REVISED_ARRIVAL_CHECK,
# MAGIC         --   NULL AS oo_PO_REVISED_ARRIVAL_CHECK
# MAGIC         details.order.revisedArrivalDate AS PO_REVISED_ARRIVAL,
# MAGIC         details.order.destinationDockId AS PO_SUGG_DOCK_DOOR,
# MAGIC         NULL AS PO_ARRIVAL_DATE_FLAG2,
# MAGIC         NULL AS PO_TRAFFIC_FAX_STATUS,
# MAGIC         NULL AS PO_ADV_SHIP_NOTICE,
# MAGIC         NULL AS PO_OTHER_UP_DOWN,
# MAGIC         NULL AS PO_OTHER_UP_DOWN_TYPE,
# MAGIC         NULL AS PO_OTHER_UP_DOWN_CODE,
# MAGIC         NULL AS PO_REPRINT_FLAG,
# MAGIC         details.order.freight.freightType AS PO_HEB_FREIGHT_TYPE,
# MAGIC         NULL AS PO_TERMS_PERCENT,
# MAGIC         NULL AS PO_TERMS_DAYS,
# MAGIC         NULL AS PO_TERMS_NET_DAYS,
# MAGIC         NULL AS PO_TERMS_PERCENT_EX,
# MAGIC         NULL AS PO_TERMS_DAYS_EX,
# MAGIC         NULL AS PO_WHSE_DISCOUNT1,
# MAGIC         NULL AS PO_WHSE_DISCOUNT2,
# MAGIC         NULL AS PO_WHSE_DISCOUNT3,
# MAGIC         NULL AS PO_WHSE_DISCOUNT4,
# MAGIC         details.order.orderedDateTime AS PO_DATE_ORDERED_MM,
# MAGIC         details.order.orderedDateTime AS PO_DATE_ORDERED_DD,
# MAGIC         details.order.orderedDateTime AS PO_DATE_ORDERED_YY,
# MAGIC         --   NULL AS PD_DATE_ORDERED_CONCAT,
# MAGIC         --   NULL AS v_PO_DATE_ORDERED_CHECK,
# MAGIC         --   NULL AS oo_PO_DATE_ORERED_CHECK,
# MAGIC         details.order.orderedDateTime AS PO_DATE_ORDERED,
# MAGIC         details.order.originalArrivalDate AS PO_DATE_ARRIVAL_MM,
# MAGIC         details.order.originalArrivalDate AS PO_DATE_ARRIVAL_DD,
# MAGIC         details.order.originalArrivalDate AS PO_DATE_ARRIVAL_YY,
# MAGIC         --   NULL AS PD_DATE_ARRIVAL_CONCAT,
# MAGIC         --   NULL AS v_PO_DATE_ARRIVAL_CHECK,
# MAGIC         --   NULL AS oo_PO_DATE_ARRIVAL_CHECK,
# MAGIC         details.order.originalArrivalDate AS PO_DATE_ARRIVAL,
# MAGIC         details.order.cancelledDateTime AS PO_DATE_CANCEL_MM,
# MAGIC         details.order.cancelledDateTime AS PO_DATE_CANCEL_DD,
# MAGIC         details.order.cancelledDateTime AS PO_DATE_CANCEL_YY,
# MAGIC         --   NULL AS PO_DATE_CANCEL_CONCAT,
# MAGIC         --   NULL AS v_CANCEL_DT_CHECK,
# MAGIC         --   NULL AS oo_CANCEL_DT_CHECK,
# MAGIC         details.order.cancelledDateTime AS PO_DATE_CANCEL,
# MAGIC         NULL AS PO_PRINT_REMARK_FLAG1,
# MAGIC         details.order.legacyData.omiRemark1 AS PO_REMARK1,
# MAGIC         NULL AS PO_PRINT_REMARK_FLAG2,
# MAGIC         details.order.legacyData.omiRemark2 AS PO_REMARK2,
# MAGIC         NULL AS PO_PRINT_REMARK_FLAG3,
# MAGIC         details.order.legacyData.omiRemark3 AS PO_REMARK3,
# MAGIC         NULL AS PO_PRINT_REMARK_FLAG4,
# MAGIC         NULL AS PO_REMARK4,
# MAGIC         NULL AS PO_FAX_STATUS,
# MAGIC         NULL AS PO_FAX_CONTACT,
# MAGIC         NULL AS PO_FAX_NUMBER1,
# MAGIC         NULL AS PO_FAX_NUMBER1_OUT,
# MAGIC         NULL AS PO_FAX_NUMBER2,
# MAGIC         NULL AS PO_FAX_NUMBER2_OUT,
# MAGIC         NULL AS PO_FAX_DATE,
# MAGIC         NULL AS PO_FAX_TIME,
# MAGIC         NULL AS PO_PRINT_REMARK_TRAFFIC_FLAG,
# MAGIC         details.order.legacyData.omiTrafficRemark AS PO_REMARK_TRAFFIC,
# MAGIC         NULL AS PO_REMARK_FREIGHT,
# MAGIC         NULL AS PO_PRINT_REMARK_FREIGHT_FLAG,
# MAGIC         NULL AS PO_REMARK_OTHER_UP_DOWN,
# MAGIC         NULL AS PO_PRINT_REMARK_OTHER_UD_IND,
# MAGIC         NULL AS PO_DETAIL_TYPE,
# MAGIC         NULL AS PO_DATE_SHIP_MM,
# MAGIC         NULL AS PO_DATE_SHIP_DD,
# MAGIC         NULL AS PO_DATE_SHIP_YY,
# MAGIC         --   NULL AS PO_DATE_SHIP_CONCAT,
# MAGIC         --   NULL AS v_PO_DATE_SHIP_CHECK,
# MAGIC         --   NULL AS oo_PO_DATE_SHIP_CHECK,
# MAGIC         NULL AS PO_DATE_SHIP,
# MAGIC         NULL AS PO_DATE_SHIP_CODE,
# MAGIC         NULL AS PO_DATE_PICKUP_MM,
# MAGIC         NULL AS PO_DATE_PICKUP_DD,
# MAGIC         NULL AS PO_DATE_PICKUP_YY,
# MAGIC         --   NULL AS PD_DATE_PICKUP_CONCAT,
# MAGIC         --   NULL AS v_PO_DATE_PICKUP_CHECK,
# MAGIC         --   NULL AS oo_PO_DATE_PICKUP_CHECK,
# MAGIC         NULL AS PO_DATE_PICKUP,
# MAGIC         NULL AS PO_DATE_PICKUP_CODE,
# MAGIC         NULL AS PO_ALT_TURN,
# MAGIC         details.order.firstReceivedDateTime AS PO_DATE_RECEIVED_MM,
# MAGIC         details.order.firstReceivedDateTime AS PO_DATE_RECEIVED_DD,
# MAGIC         details.order.firstReceivedDateTime AS PO_DATE_RECEIVED_YY,
# MAGIC         --   NULL AS PD_DATE_RECEIVED_CONCAT,
# MAGIC         --   NULL AS v_PO_DATE_RECEIVED_CHECK,
# MAGIC         --   NULL AS oo_PO_DATE_RECEIVED_CHECK,
# MAGIC         details.order.firstreceiveddatetime AS PO_DATE_RECEIVED,
# MAGIC         details.order.warehouse.legacyWarehouseCode AS PO_WAREHOUSE_CODE,
# MAGIC         NULL AS PO_NUMBER_OF_PAGES,
# MAGIC         NULL AS PO_REVISED_FLAG,
# MAGIC         --details.order.items.backorderAllowed AS PO_BACKORDER_ALLOWED_IND,
# MAGIC         null as PO_BACKORDER_ALLOWED_IND,
# MAGIC         details.order.orderCosts.prepayAndAdd.quotedDecimal AS PO_PREPAY_AND_ADD,
# MAGIC         NULL AS PO_PREPAY_AND_ADD_TYPE,
# MAGIC         NULL AS PO_PP_AND_ADD_EX,
# MAGIC         NULL AS PO_PP_AND_ADD_EX_TYPE,
# MAGIC         NULL AS PO_TURN_DOZENS,
# MAGIC         NULL AS PO_TURN_DOZENS_ALT,
# MAGIC         NULL AS PO_ACTUAL_TURN_DOZENS,
# MAGIC         NULL AS PO_PROMO_DOZENS,
# MAGIC         NULL AS PO_FWD_BUY_DOZENS,
# MAGIC         NULL AS PO_TERMS_BASE,
# MAGIC         details.order.orderCosts.freightCharge.quotedDecimal AS PO_FREIGHT_BILL,
# MAGIC         details.order.orderCosts.freightChargeType AS PO_FREIGHT_BILL_TYPE,
# MAGIC         NULL AS PO_BACKHAUL,
# MAGIC         NULL AS PO_BACKHAUL_TYPE,
# MAGIC         details.order.legacyData.prepayToCollectFlag AS PO_FLAG_PREPAID,
# MAGIC         NULL AS PO_FLAG_PREPAY_AND_ADD,
# MAGIC         NULL AS PO_FLAG_FREIGHT_BILL,
# MAGIC         NULL AS PO_FLAG_BACKHAUL,
# MAGIC         NULL AS PO_FLAG_TRUCK,
# MAGIC         NULL AS PO_FLAG_RAIL,
# MAGIC         NULL AS PO_FLAG_AIR,
# MAGIC         NULL AS PO_FLAG_WATER,
# MAGIC         NULL AS PO_CONSOLIDATED_ORDER_NUMBER,
# MAGIC         NULL AS PO_PENDING_FLAG,
# MAGIC         details.order.deliveryAppointmentDateTime AS PO_DATE_APPOINTMENT_MM,
# MAGIC         details.order.deliveryAppointmentDateTime AS PO_DATE_APPOINTMENT_DD,
# MAGIC         details.order.deliveryAppointmentDateTime AS PO_DATE_APPOINTMENT_YY,
# MAGIC         --   NULL AS PD_DATE_APPOINTMENT_CONCAT,
# MAGIC         --   NUL AS v_PO_DATE_APPOINTMENT_CHECK,
# MAGIC         --   NUL AS OO_PO_DATE_APPOINTMENT_CHECK,
# MAGIC         details.order.deliveryAppointmentDateTime AS PO_DATE_APPOINTMENT,
# MAGIC         details.order.deliveryAppointmentDateTime AS PO_TIME_APPOINTMENT_HH,
# MAGIC         details.order.deliveryAppointmentDateTime AS PO_TIME_APPOINTMENT_MM,
# MAGIC         details.order.deliveryAppointmentDateTime AS PO_TIME_APPOINTMENT,
# MAGIC         NULL AS PO_BUYER_PRIORITY,
# MAGIC         NULL AS PO_CALCULATED_PRIORITY,
# MAGIC         details.order.supplier.supplierApNumber AS PO_AP_VENDOR_NUMBER,
# MAGIC         NULL AS PO_PF7_TRIGGERED,
# MAGIC         NULL AS PO_PF8_PULLED,
# MAGIC         NULL AS PO_PF11_MINIMUM,
# MAGIC         NULL AS PO_PF12_MAXIMUM,
# MAGIC         NULL AS PO_PF13_WORK,
# MAGIC         NULL AS PO_PF14_ACTIVE,
# MAGIC         NULL AS PO_COSTS_REVIEWED,
# MAGIC         NULL AS PO_COSTS_REVIEWED_USER_ID,
# MAGIC         NULL AS PO_BILL_FOR_SHORTED_DEALS_FLAG,
# MAGIC         details.order.supplier.laneOriginZipCode AS PO_PICKUP_POINT_ZIP1,
# MAGIC         details.order.supplier.laneOriginZipCode AS PO_PICKUP_POINT_ZIP2,
# MAGIC         details.order.supplier.laneOriginZipCode AS PO_PICKUP_POINT_ZIP3,
# MAGIC         details.order.supplier.laneOriginZipCode AS PO_PICKUP_POINT_ZIP4,
# MAGIC         NULL AS PO_HEB_FREIGHT_BILL_TYPE,
# MAGIC         NULL AS PO_HEB_FREIGHT_BILL_TYPE_A,
# MAGIC         NULL AS PO_HEB_FREIGHT_BILL_TYPE_B,
# MAGIC         NULL AS PO_HEB_BACKHAUL_TYPE,
# MAGIC         NULL AS PO_HEB_UCS_VENDOR_FLAG,
# MAGIC         NULL AS PO_HEB_LTL_FLAG,
# MAGIC         NULL AS PO_HEB_RECEIVING_STATUS,
# MAGIC         NULL AS PO_HEB_WORK_IN_PROGRESS,
# MAGIC         NULL AS SW_IMPORT,
# MAGIC         details.order.orderState AS ACTION,
# MAGIC         NULL AS TS_LD,
# MAGIC         details.order.inventoryAnalystNumber AS PO_BUYER_NUMBER,
# MAGIC         details.order.wimsId AS PO_WIMS_ID,
# MAGIC         metadata.emittedAtDateTime as EMITTEDATDATETIME,
# MAGIC         metadata.uuid AS UUID,
# MAGIC         CURRENT_DATE AS DSSC_LD_DT,
# MAGIC ----ITEM LVL AGG FOR HDR EXAMPLE
# MAGIC         ---
# MAGIC           AGGREGATE(
# MAGIC   --ITEM_CUBE_ARRAY
# MAGIC     zip_with(
# MAGIC     --NVL_FOR_QTY_ARRAY
# MAGIC       zip_with(
# MAGIC         details.order.items.revisedTurnQuantity,
# MAGIC         details.order.items.originalTurnQuantity,(x, y) -> (nvl(x, y))
# MAGIC       ),
# MAGIC       details.order.items.itemDimensions.volumeInTenThousandthsCube,(x, y) -> (y * x / 10000)
# MAGIC     ),
# MAGIC     cast(0 as DOUBLE),
# MAGIC     (acc, x) -> acc + x
# MAGIC   ) HDR_TURN_CUBE_EXAMPLE
# MAGIC       ) hdr,
# MAGIC       -----STRUT FOR DTL
# MAGIC       struct(
# MAGIC         wimsid,
# MAGIC         itemCode,
# MAGIC         -- count(receiptNumbers)
# MAGIC         itemDimensions.masterPack,
# MAGIC         itemDimensions.packSize,
# MAGIC         itemDescription,
# MAGIC         originalPromoQuantity,
# MAGIC         originalForwardBuyQuantity,
# MAGIC         itemCosts.listCostAmount.quotedDecimal,
# MAGIC         itemCosts.offInvoiceAmount.quotedDecimal,
# MAGIC         itemCosts.itemUpDownAmount.quotedDecimal,
# MAGIC         itemCosts.paymentTermDiscountAmount.quotedDecimal,
# MAGIC         itemCosts.freightAllowanceAmount.quotedDecimal,
# MAGIC         itemCosts.supplierUpDownAmount.quotedDecimal,
# MAGIC         itemCosts.prepayAndAddAmount.quotedDecimal,
# MAGIC         itemCosts.billbackAmount.quotedDecimal,
# MAGIC         itemCosts.itemUpDownComment,
# MAGIC         itemDimensions.weightInTenThousandthsLbs,
# MAGIC         itemDimensions.volumeInTenThousandthsCube,
# MAGIC         itemDimensions.palletQuantity,
# MAGIC         orderingUPC,
# MAGIC         itemDimensions.freightUnit,
# MAGIC         itemDimensions.shippingPackBreakdown,
# MAGIC         receivedQuantity,
# MAGIC         backorderAllowed,
# MAGIC         itemCosts.lastCostAmount.quotedDecimal,
# MAGIC         itemClass,
# MAGIC         itemCosts.freightBillAmount.quotedDecimal,
# MAGIC         itemCosts.backhaulAmount.quotedDecimal,
# MAGIC         firstReceivedDateTime,
# MAGIC         itemDimensions.variableWeightIndicator,
# MAGIC         receivedQuantity,
# MAGIC         receivedDamagedQuantity,
# MAGIC         originalTurnQuantity,
# MAGIC         revisedTurnQuantity,
# MAGIC         revisedForwardBuyQuantity,
# MAGIC         revisedPromoQuantity,
# MAGIC         originalTotalOrderedQuantity,
# MAGIC         revisedTotalOrderedQuantity,
# MAGIC         itemDimensions.palletTie,
# MAGIC         itemDimensions.palletTier,
# MAGIC         receiptNumbers,
# MAGIC       --  inline(receiptNumbers) LAST_receiptNumbers,
# MAGIC         --receipts,
# MAGIC         nvl(
# MAGIC           revisedTotalOrderedQuantity,
# MAGIC           originalTotalOrderedQuantity
# MAGIC         ) TotalOrderedQuantity,
# MAGIC         nvl(revisedTurnQuantity, originalTurnQuantity) TotalTurnQuantity,
# MAGIC         nvl(
# MAGIC           revisedForwardBuyQuantity,
# MAGIC           originalForwardBuyQuantity
# MAGIC         ) TotalForwardBuyQuantity,
# MAGIC         nvl(revisedPromoQuantity, originalPromoQuantity) TotalPromoQuantity,
# MAGIC         null as ok
# MAGIC       ) as dtl
# MAGIC     from
# MAGIC       (
# MAGIC         select
# MAGIC           *,
# MAGIC           inline(details.order.items)
# MAGIC         from
# MAGIC           delta.`s3a://crt-supply-chain-datalake/s595730/wims/structreWIMS/`
# MAGIC         where
# MAGIC           bi_act_sw = true 
# MAGIC          and po_stat = 'A'
# MAGIC           -- and receiptNumbers is not null
# MAGIC           --and wimsid = 'L2011-037-681360'
# MAGIC       )
# MAGIC   ) 
# MAGIC --   where 1=0
# MAGIC  -- where hdr.PO_VENDOR_FACILITY = 47
# MAGIC   
# MAGIC   --       group by wimsid,
# MAGIC   -- itemCode
# MAGIC   ---display(df)
# MAGIC   -- where 1=1 and receiptNumbers is not null

# COMMAND ----------

# MAGIC %sql
# MAGIC 
# MAGIC select dtl.* from 
# MAGIC s595730.wimspo

# COMMAND ----------

# MAGIC %sql
# MAGIC 
# MAGIC 
# MAGIC select * from delta.`s3a://stg-enc-data/supply_chain_stage/structured_wims_json`
# MAGIC --where details.order.wimsid = 'L2010-006-599220'
